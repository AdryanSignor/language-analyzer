void main() {
	int a = 1;

	//AUTO_INCREMENT
	a++;
	a--;

	//OPERATIONS
	a + 1;
	a - 1;
	a * 1;
	a / 1;
	a % 1;

	//ASSING
	a = 1;
	a += 1;
	a -= 1;
	a *= 1;
	a /= 1;
	a %= 1;

	// CONST
	3;
	33;
	33.;
	3.0;
	.01;

	//DECLARATION
	int _;
	int __;
	int _b_b;
	int _8_9;
	int _8;
	int b;
	int bbb;
	int bb88;
	int A;

	// TYPES
	short d;
	short int dd;
	signed short ddd;
	signed short int dddd;
	unsigned short int ddddd;
	unsigned short int dddddd;

	int ddddddd;
	signed dddddddd;
	signed int ddddddddd;

	long n;
	float z;
	double y;

	// LOGIC
	1 && 1;
	1 || 1;
	1 == 1;
	1 != 1;
	1 >= 1;
	1 <= 1;
	1 < 1;
	1 > 1;
	!1;

	!!1;

	//BIT
	int c = 1;
	c <<= 1;

	// Fail
	// . 1;
	// .;
	// int 9_;
	// 9 9;
}