#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "constant/constants.h"
#include "constant/tokens.h"
#include "constant/status.h"

#include "global/globals.h"

#include "utils/is_token.h"
#include "utils/is_next.h"
#include "utils/file_reader.h"
#include "utils/reserved_word.h"
#include "utils/get_token_name.h"

int read_token() {
	int posl = 0;
	int status = STATUS_Q0;

	while (1) {
		char c = lexical[posl++] = program[pos++];

		if (c == '\n') {
			row++;
			column = 1;
		} else {
			column = (c == '\t' ? column + 4 : column + 1);
		}

		switch(status) {
			case STATUS_Q0:
				if (isalpha(c) || c == '_') {
					status = STATUS_Q28_RESERVED_WORD;
					break;
				}

				if (isdigit(c)) {
					status = STATUS_Q1_CONST;
					break;
				}

				if (is_token(c, '.', posl)) {
					c = program[pos];
					if (isdigit(c)) {
						status = STATUS_Q2_CONST;
						lexical[posl++] = c;
						pos++;
						break;
					} else {
						return TK_UNEXPECTED;
					}
				}

				if (is_token(c, '/', posl)) {
					c = program[pos++];
					if (is_token(c, '=', posl + 1)) {
						return TK_DIV_EQUAL;
					} else if (c == '/') {
						status = STATUS_Q46_COMMENT;
						break;
					} else if (c == '*') {
						status = STATUS_Q47_Q48_Q49_Q50_COMMENT;
						break;
					} else {
						column--;
						pos--;
						return TK_DIV;
					}
				}

				if (is_token(c, '>', posl)) {
					if (is_next('>', posl)) {
						return (is_next('=', posl + 1) ? TK_SHIFT_RIGHT_EQUAL : TK_SHIFT_RIGHT);
					} else if (is_next('=', posl)) {
						return TK_IS_GREATER_OR_EQUAL;
					} else {
						return TK_IS_GREATER;
					}
				}

				if (is_token(c, '<', posl)) {
					if (is_next('<', posl)) {
						return (is_next('=', posl + 1) ? TK_SHIFT_LEFT_EQUAL : TK_SHIFT_LEFT);
					} else if (is_next('=', posl)) {
						return TK_IS_SMALLER_OR_EQUAL;
					} else {
						return TK_IS_SMALLER;
					}
				}

				if (is_token(c, '-', posl)) {
					return (is_next('-', posl) ? TK_AUTO_DEC : (is_next('=', posl) ? TK_MINUS_EQUAL : TK_MINUS));
				}

				if (is_token(c, '+', posl)) {
					return (is_next('+', posl) ? TK_AUTO_INC : (is_next('=', posl) ? TK_PLUS_EQUAL : TK_PLUS));
				}

				if (is_token(c, '&', posl)) {
					return (is_next('&', posl) ? TK_LOGICAL_AND : (is_next('=', posl) ? TK_AND_EQUAL : TK_BIT_AND));
				}

				if (is_token(c, '|', posl)) {
					return (is_next('|', posl) ? TK_LOGICAL_OR : (is_next('=', posl) ? TK_OR_EQUAL : TK_BIT_OR));
				}

				if (is_token(c, '^', posl)) {
					return (is_next('=', posl) ? TK_XOR_EQUAL : TK_BIT_XOR);
				}

				if (is_token(c, '%', posl)) {
					return (is_next('=', posl) ? TK_MOD_EQUAL : TK_MOD);
				}

				if (is_token(c, '!', posl)) {
					return (is_next('=', posl) ? TK_IS_NOT_EQUAL : TK_LOGICAL_NOT);
				}

				if (is_token(c, '*', posl)) {
					return (is_next('=', posl) ? TK_MULT_EQUAL : TK_MULT);
				}

				if (is_token(c, '=', posl)) {
					return (is_next('=', posl) ? TK_IS_EQUAL : TK_EQUAL);
				}

				if (is_token(c, '(', posl)) {
					return TK_PARENTHESES_OPEN;
				}

				if (is_token(c, ')', posl)) {
					return TK_PARENTHESES_CLOSE;
				}

				if (is_token(c, '{', posl)) {
					return TK_CURLY_BRACES_OPEN;
				}

				if (is_token(c, '}', posl)) {
					return TK_CURLY_BRACES_CLOSE;
				}

				if (is_token(c, ',', posl)) {
					return TK_COMMA;
				}

				if (is_token(c, ';', posl)) {
					return TK_SEMICOLON;
				}

				if (is_token(c, ':', posl)) {
					return TK_COLON;
				}

				if (is_token(c, '?', posl)) {
					return TK_QUESTION;
				}

				if (is_token(c, '~', posl)) {
					return TK_BIT_COMPLEMENT;
				}

				if (c == ' ' || c == '\n' || c == '\t') {
					posl--;
					break;
				}

				return (c == '\0' ? END_FILE : TK_UNEXPECTED);

			case STATUS_Q28_RESERVED_WORD:
				if (isalpha(c) || isdigit(c) || c == '_' ) {
					break;
				} else {
					column--;
					pos--;
				}

				lexical[--posl] = '\0';
				return reserved_word(lexical);

			case STATUS_Q46_COMMENT:
				if (c == '\n') {
					posl = 0;
					status = STATUS_Q0;
				} else if (c == '\0') {
					return END_FILE;
				}
				break;

			case STATUS_Q47_Q48_Q49_Q50_COMMENT:
				if (c == '*' && is_next('/', posl)) {
					posl = 0;
					status = STATUS_Q0;
				} else if (c == '\0') {
					return END_FILE;
				}
				break;

			case STATUS_Q1_CONST:
				if (c == '.') {
					status = STATUS_Q2_CONST;
				} else if (!isdigit(c)) {
					lexical[--posl] = '\0';
					column--;
					pos--;
					return TK_CONSTANT;
				}
				break;

			case STATUS_Q2_CONST:
				if (isdigit(c)) {
					break;
				}
				lexical[--posl] = '\0';
				return TK_CONSTANT;
		}
	}
}

int main(int argc, char *argv[]) {
	read_file();

	FILE *output;
	output = fopen("result/Saida.lex", "w");

	int tk;
	while (tk = read_token()) {
		fprintf(output, "l:%-3.d c:%-3.d %-26s(q%2.d) %s\n", row, (column - strlen(lexical)), get_token_name(tk), tk, lexical);
	}

	fclose(output);
	printf("Escrito em result/Saida.lex\n");
}