#define TK_UNEXPECTED 3

#define TK_ID 28

#define TK_CONSTANT 2

#define TK_PLUS 18
#define TK_MINUS 17
#define TK_DIV 15
#define TK_MULT 16
#define TK_MOD 14

#define TK_EQUAL 13
#define TK_PLUS_EQUAL 10
#define TK_MINUS_EQUAL 45
#define TK_DIV_EQUAL 43
#define TK_MULT_EQUAL 44
#define TK_MOD_EQUAL 42

#define TK_AUTO_INC 20
#define TK_AUTO_DEC 19

#define TK_IS_EQUAL 41
#define TK_IS_NOT_EQUAL 40
#define TK_IS_GREATER 6
#define TK_IS_SMALLER 7
#define TK_IS_GREATER_OR_EQUAL 37
#define TK_IS_SMALLER_OR_EQUAL 38

#define TK_LOGICAL_NOT 8
#define TK_LOGICAL_OR 31
#define TK_LOGICAL_AND 9

#define TK_BIT_AND 4
#define TK_BIT_OR 5
#define TK_BIT_XOR 33
#define TK_BIT_COMPLEMENT 32
#define TK_AND_EQUAL 30
#define TK_OR_EQUAL 35
#define TK_XOR_EQUAL 29

#define TK_SHIFT_LEFT 12
#define TK_SHIFT_RIGHT 11
#define TK_SHIFT_LEFT_EQUAL 39
#define TK_SHIFT_RIGHT_EQUAL 36

#define TK_COMMA 27
#define TK_QUESTION 34
#define TK_PARENTHESES_OPEN 21
#define TK_CURLY_BRACES_OPEN 23
#define TK_PARENTHESES_CLOSE 22
#define TK_CURLY_BRACES_CLOSE 24
#define TK_SEMICOLON 25
#define TK_COLON 26

#define TK_WHILE 51
#define TK_DO 52
#define TK_IF 53
#define TK_ELSE 54
#define TK_SWITCH 55
#define TK_CASE 56
#define TK_DEFAULT 57
#define TK_FOR 58

#define TK_SIGNED 60
#define TK_UNSIGNED 61
#define TK_VOID 62
#define TK_SHORT 63
#define TK_INT 64
#define TK_LONG 65
#define TK_FLOAT 66
#define TK_DOUBLE 67