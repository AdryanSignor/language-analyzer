int is_token(char next, char expected, int posl) {
	int is = (next == expected);
	if (is) {
		lexical[posl] = '\0';
	}

	return is;
}