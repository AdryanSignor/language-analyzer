typedef struct {
	char word[20];
	int tk;
} ReservedWord;

int reserved_word(char lexical[]) {
	ReservedWord wordList[] = {
		{"for", TK_FOR},
		{"while", TK_WHILE},
		{"do", TK_DO},
		{"if", TK_IF},
		{"else", TK_ELSE},
		{"switch", TK_SWITCH},
		{"case", TK_CASE},
		{"default", TK_DEFAULT},
		{"signed", TK_SIGNED},
		{"unsigned", TK_UNSIGNED},
		{"void", TK_VOID},
		{"short", TK_SHORT},
		{"int", TK_INT},
		{"long", TK_LONG},
		{"float", TK_FLOAT},
		{"double", TK_DOUBLE}
	};

	int tk = TK_ID;
	int i = sizeof(wordList) / sizeof(wordList[0]);

	for (; i >= 0; i--) {
		if (strcmp(lexical, wordList[i].word) == 0) {
			tk = wordList[i].tk;
			break;
		}
	}

	return tk;
}