int validateExtension(char path[], char extensionsValids[][LENGTH_EXTENSION], int numExtensions) {
	int isValid = 0;
	char *fileExtension;

	if (numExtensions > 0) {
		if ((fileExtension = strrchr(path, '.')) != NULL) {
			for (int i = 0; i < numExtensions; i++) {
				if (strcmp(fileExtension, extensionsValids[i]) == 0) {
					isValid = 1;
					break;
				}
			}
		}
	} else {
		isValid = 1;
	}

	return isValid;
}

FILE* file_reader(char readMode[2], char extensionsValids[][LENGTH_EXTENSION], int numExtensions) {
	char path[BUFFERSIZE_PATH];
	char *endString;

	int isRead = 0;

	FILE *file;

	do {
		printf("Digite o nome/caminho do arquivo ou q para sair:\n");
		while(!fgets(path, BUFFERSIZE_PATH, stdin)) {
			printf("Erro ao ler o caminho do arquivo!\n");
		}

		if ((endString = strchr(path, '\n')) != NULL) {
			*endString = '\0';
		}

		if (!INSENSITIVE_CASE_COMPARE(path, "q")) {
			exit(1);
		}

		if (validateExtension(path, extensionsValids, numExtensions)) {
			if ((file = fopen(path, readMode)) != NULL) {
				isRead = 1;
			} else {
				printf("O arquivo não pode ser aberto!\n");
			}
		} else {
			printf("Extensões válidas: %s", extensionsValids[0]);
			for (int i = 1; i < numExtensions; i++) {
				printf(", %s", extensionsValids[i]);
			}
			printf("\n");
		}
	} while(!isRead);

	return file;
}

void read_file() {
	FILE *e;

	char extensionsValids[][LENGTH_EXTENSION] = {".c"};
	e = file_reader("r", extensionsValids, 1);

	char c;
	while ((c = fgetc(e)) != EOF) {
		program[pos++] = c;
	}
	program[pos] = '\0';
	pos = 0;

	fclose(e);
}