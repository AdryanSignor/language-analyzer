O presente trabalho, desenvolvido por Adryan Alessandro Signor, sob coordenação do prof. Ricardo Vargas Dorneles, realiza a leia um arquivo fonte em C e efetue a análise léxica do mesmo.

Observações:
SO: linux;
Os arquivos de detalhamento do projeto, arquitetura e autômato encontram-se no diretório doc;

Instruções
1 - Inicie a aplicação language_analyzer (./language_analyzer);
2 - Digite o caminho do arquivo ou o arquivo .c (caso encontre-se no mesmo diretório);
3 - Um arquivo será gerado no diretório result, com o nome Saida.lex;
